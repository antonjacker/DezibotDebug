#include <DezibotDebug.h>
#include <ESP8266mDNS.h>
#include <ESP8266WiFi.h>
const char *ssid = "...";
const char *password = "...";
DezibotDebug Debug;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  Serial.println();
  Serial.print("Configuring wifi...");
  /* You can remove the password parameter if you want the AP to be open. */
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  if (!MDNS.begin("dezibot")) {             // Start the mDNS responder for esp8266.local
    Serial.println("Error setting up MDNS responder!");
  }
  IPAddress myIP = WiFi.localIP();
  Serial.print("IP address: ");
  Serial.println(myIP);
  Debug.begin("dezibot");
}

void loop() {
  // put your main code here, to run repeatedly:
  Debug.handle();
}
